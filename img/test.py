import pyautogui as py

im1 = py.screenshot('img/screen.png')

redactarBtnLocation = py.locateOnScreen('img/redactar-btn.png')

redactarBtnPoint = py.locateCenterOnScreen('img/redactar-btn.png')

redactarBtnX = redactarBtnPoint.x

redactarBtnY = redactarBtnPoint.y

py.click(redactarBtnX, redactarBtnY)

py.typewrite('rdayis', interval=.2)

email = py.locateCenterOnScreen('img/dayisMail.png')

emailX = email.x

emailY = email.y

py.click(emailX, emailY)

asunto = py.locateCenterOnScreen('img/asunto.png')

asuntoX = asunto.x

asuntoY = asunto.y

py.click(asuntoX, asuntoY)

py.typewrite('Test de Prueba', interval=.2)
